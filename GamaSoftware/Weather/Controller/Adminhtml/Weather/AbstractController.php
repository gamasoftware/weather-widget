<?php declare(strict_types = 1);
/**
 * @category   GamaSoftware
 * @package    GamaSoftware_Weather
 * @subpackage Controller
 * @author     Grzegorz Rzeźnikiewicz <grzegorz.rzeznikiewicz@gmail.com>
 * @copyright  Copyright (c) 2020 Gama Software
 * @since      1.0.0
 */

namespace GamaSoftware\Weather\Controller\Adminhtml\Weather;

use Magento\Backend\App\Action;

/**
 * Class AbstractController
 *
 * @package GamaSoftware\Weather\Controller\Adminhtml\Weather
 */
abstract class AbstractController extends Action
{
    public const ADMIN_RESOURCE = 'GamaSoftware_Weather::gama_software_weather';
}
