<?php declare(strict_types = 1);
/**
 * @category   GamaSoftware
 * @package    GamaSoftware_Weather
 * @subpackage Block
 * @author     Grzegorz Rzeźnikiewicz <grzegorz.rzeznikiewicz@gmail.com>
 * @copyright  Copyright (c) 2020 Gama Software
 * @since      1.0.0
 */

namespace GamaSoftware\Weather\Block;

use GamaSoftware\Weather\Api\Data\WeatherInterface;
use GamaSoftware\Weather\Api\Data\WeatherRepositoryInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

/**
 * Class Weather
 *
 * @package GamaSoftware\Weather\Block
 */
class Weather extends Template
{
    /**
     * @var WeatherRepositoryInterface
     */
    protected $weatherRepository;

    /**
     * Weather constructor.
     *
     * @param Context                    $context
     * @param WeatherRepositoryInterface $weatherRepository
     * @param array                      $data
     */
    public function __construct(Context $context, WeatherRepositoryInterface $weatherRepository, array $data = [])
    {
        parent::__construct($context, $data);

        $this->weatherRepository = $weatherRepository;
    }

    /**
     * @return WeatherInterface
     */
    public function getLastWeather(): WeatherInterface
    {
        return $this->weatherRepository->getLast();
    }
}
