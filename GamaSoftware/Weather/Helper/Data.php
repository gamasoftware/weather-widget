<?php

namespace GamaSoftware\Weather\Helper;

use GamaSoftware\Weather\Api\Data\WeatherInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;

/**
 * Class Data
 *
 * @package GamaSoftware\Weather\Helper
 */
class Data extends AbstractHelper
{
    /**
     * @var TimezoneInterface
     */
    private $magentoDate;

    /**
     * Data constructor.
     *
     * @param Context           $context
     * @param TimezoneInterface $magentoDate
     */
    public function __construct(Context $context, TimezoneInterface $magentoDate)
    {
        parent::__construct($context);
        $this->magentoDate = $magentoDate;
    }

    /**
     * @param array
     *
     * @return array
     */
    public function prepareData(array $data): array
    {
        $currentStoreDate = $this->magentoDate->date();
        $currentStoreDate = $currentStoreDate->format('Y-m-d H:m:s');

        return [
            WeatherInterface::DATE         => $currentStoreDate,
            WeatherInterface::LOCALIZATION => $data['name'],
            WeatherInterface::TEMP         => $data['main']['temp'],
            WeatherInterface::TEMP_MIN     => $data['main']['temp_min'],
            WeatherInterface::TEMP_MAX     => $data['main']['temp_max'],
            WeatherInterface::PRESSURE     => $data['main']['pressure'],
            WeatherInterface::HUMIDITY     => $data['main']['humidity'],
            WeatherInterface::SUNRISE      => $data['sys']['sunrise'],
            WeatherInterface::SUNSET       => $data['sys']['sunset'],
            WeatherInterface::CLOUDS       => $data['clouds']['all'],
            WeatherInterface::VISIBILITY   => $data['visibility'],
            WeatherInterface::WIND_SPEED   => $data['wind']['speed'],
            WeatherInterface::WIND_DEG     => $data['wind']['deg'],
            WeatherInterface::DESCRIPTION  => $data['weather'][0]['description'],
            WeatherInterface::MAIN         => $data['weather'][0]['main'],
            WeatherInterface::ICON         => $data['weather'][0]['icon']
        ];
    }
}
