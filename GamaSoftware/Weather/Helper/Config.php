<?php declare(strict_types = 1);
/**
 * @category   GamaSoftware
 * @package    GamaSoftware_Weather
 * @subpackage Helper
 * @author     Grzegorz Rzeźnikiewicz <grzegorz.rzeznikiewicz@gmail.com>
 * @copyright  Copyright (c) 2020 Gama Software
 * @since      1.0.0
 */

namespace GamaSoftware\Weather\Helper;

use Magento\Framework\App\Config\ScopeConfigInterface;

/**
 * Class Config
 *
 * @package GamaSoftware\Weather\Helper
 */
class Config
{
    /** @var string */
    public const WEATHER_ENABLED_PATH = 'weather/general/enabled';
    /** @var string */
    public const WEATHER_CRON_ENABLED_PATH = 'weather/general/cron_enabled';
    /** @var string */
    public const WEATHER_API_KEY_PATH = 'weather/general/api_key';
    /** @var string */
    public const WEATHER_CITY_PATH = 'weather/general/city';
    /** @var string */
    public const WEATHER_COUNTRY_PATH = 'weather/general/country';

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(ScopeConfigInterface $scopeConfig)
    {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->scopeConfig->isSetFlag(static::WEATHER_ENABLED_PATH);
    }

    /**
     * @return bool
     */
    public function isCronEnabled(): bool
    {
        return $this->scopeConfig->isSetFlag(static::WEATHER_CRON_ENABLED_PATH);
    }

    /**
     * @return string|null
     */
    public function getApiKey(): ?string
    {
        return $this->scopeConfig->getValue(static::WEATHER_API_KEY_PATH);
    }

    /**
     * @return string|null
     */
    public function getCity(): ?string
    {
        return $this->scopeConfig->getValue(static::WEATHER_CITY_PATH);
    }

    /**
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return $this->scopeConfig->getValue(static::WEATHER_COUNTRY_PATH);
    }
}
