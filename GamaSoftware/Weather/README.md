# Weather Widget - Magento 2 module

## Requirements:
1. Magento 2.3.x or newer
2. API key of OpenWeather Map service:
https://openweathermap.org/

## Installation:
```
bin/magento enable:GamaSoftware_Weather
bin/magento setup:upgrade
bin/magento cache:clean
``` 

### Configuration:
1. Go to admin panel -> Gama Software -> Configuration -> Weather Widget
2. Enable Weather widget
3. Enable cron job for Weather Widget - it will be executed every 10 minutes
4. Put Your API key
5. Put city and country - default Lublin and pl

### Usage:
Run:
`bin/magento gama_software:update-weather` 
to get current weather

### Weather Archive
Go to admin panel -> Gama Software -> Weather Archive to see all weather getting from API