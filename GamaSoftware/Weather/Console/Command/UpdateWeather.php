<?php declare(strict_types = 1);
/**
 * @category   GamaSoftware
 * @package    GamaSoftware_Weather
 * @subpackage Console
 * @author     Grzegorz Rzeźnikiewicz <grzegorz.rzeznikiewicz@gmail.com>
 * @copyright  Copyright (c) 2020 Gama Software
 * @since      1.0.0
 */

namespace GamaSoftware\Weather\Console\Command;

use Exception;
use GamaSoftware\Weather\Api\Data\WeatherInterfaceFactory;
use GamaSoftware\Weather\Api\Data\WeatherRepositoryInterface;
use GamaSoftware\Weather\Helper\Config;
use GamaSoftware\Weather\Helper\Data as Helper;
use GamaSoftware\Weather\Model\OpenWeatherMap;
use Magento\Framework\Console\Cli;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Zend_Http_Client_Exception;

/**
 * Class UpdateWeather
 *
 * @package GamaSoftware\Weather\Console\Command
 */
class UpdateWeather extends Command
{
    public const COMMAND_NAME = 'gama_software:update-weather';

    /**
     * @var WeatherRepositoryInterface
     */
    protected $weatherRepositoryInterface;

    /**
     * @var WeatherInterfaceFactory
     */
    protected $weatherRepositoryFactory;

    /**
     * @var OpenWeatherMap
     */
    protected $openWeatherMap;

    /**
     * @var Helper
     */
    protected $helper;

    /**
     * @var Config
     */
    protected $config;

    /**
     * ResetPoints constructor.
     *
     * @param WeatherRepositoryInterface $weatherRepositoryInterface
     * @param WeatherInterfaceFactory    $weatherRepositoryFactory
     * @param OpenWeatherMap             $openWeatherMap
     * @param Helper                     $helper
     * @param Config                     $config
     * @param string|null                $name
     */
    public function __construct(
        WeatherRepositoryInterface $weatherRepositoryInterface,
        WeatherInterfaceFactory $weatherRepositoryFactory,
        OpenWeatherMap $openWeatherMap,
        Helper $helper,
        Config $config,
        string $name = null
    ) {
        parent::__construct($name);

        $this->weatherRepositoryInterface = $weatherRepositoryInterface;
        $this->weatherRepositoryFactory   = $weatherRepositoryFactory;
        $this->openWeatherMap             = $openWeatherMap;
        $this->helper                     = $helper;
        $this->config                     = $config;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|null
     * @throws Exception
     */
    public function execute(InputInterface $input, OutputInterface $output): ?int
    {
        if ($this->config->isEnabled()) {
            throw new Exception('Module is not active');
        }

        try {
            $weatherDataFromAPI = $this->openWeatherMap->getWeather();
            $weather = $this->weatherRepositoryFactory->create();
            $weather->setData($this->helper->prepareData($weatherDataFromAPI));
            $this->weatherRepositoryInterface->save($weather);

            return Cli::RETURN_SUCCESS;
        } catch (Zend_Http_Client_Exception $exception) {
            return Cli::RETURN_FAILURE;
        } catch (Exception $exception) {
            return Cli::RETURN_FAILURE;
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName(self::COMMAND_NAME)
             ->setDescription('Update weather from API');
    }
}
