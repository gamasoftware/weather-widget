<?php
use \Magento\Framework\Component\ComponentRegistrar;
ComponentRegistrar::register(
    ComponentRegistrar::MODULE,
    'GamaSoftware_Weather',
    __DIR__
);
