<?php declare(strict_types = 1);
/**
 * @category   GamaSoftware
 * @package    GamaSoftware_Weather
 * @subpackage Cron
 * @author     Grzegorz Rzeźnikiewicz <grzegorz.rzeznikiewicz@gmail.com>
 * @copyright  Copyright (c) 2020 Gama Software
 * @since      1.0.0
 */

namespace GamaSoftware\Weather\Cron;

use GamaSoftware\Weather\Api\Data\WeatherInterfaceFactory;
use GamaSoftware\Weather\Api\Data\WeatherRepositoryInterface;
use GamaSoftware\Weather\Helper\Config;
use GamaSoftware\Weather\Helper\Data as Helper;
use GamaSoftware\Weather\Model\OpenWeatherMap;
use Zend_Http_Client_Exception;

/**
 * Class UpdateWeather
 *
 * @package GamaSoftware\Weather\Cron
 */
class UpdateWeather
{
    /**
     * @var WeatherRepositoryInterface
     */
    protected $weatherRepositoryInterface;

    /**
     * @var WeatherInterfaceFactory
     */
    protected $weatherRepositoryFactory;

    /**
     * @var OpenWeatherMap
     */
    protected $openWeatherMap;

    /**
     * @var Helper
     */
    protected $helper;

    /**
     * @var Config
     */
    protected $config;

    /**
     * ResetPoints constructor.
     *
     * @param WeatherRepositoryInterface $weatherRepositoryInterface
     * @param WeatherInterfaceFactory    $weatherRepositoryFactory
     * @param OpenWeatherMap             $openWeatherMap
     * @param Helper                     $helper
     * @param Config                     $config
     */
    public function __construct(
        WeatherRepositoryInterface $weatherRepositoryInterface,
        WeatherInterfaceFactory $weatherRepositoryFactory,
        OpenWeatherMap $openWeatherMap,
        Helper $helper,
        Config $config
    ) {
        $this->weatherRepositoryInterface = $weatherRepositoryInterface;
        $this->weatherRepositoryFactory   = $weatherRepositoryFactory;
        $this->openWeatherMap             = $openWeatherMap;
        $this->helper                     = $helper;
        $this->config                     = $config;
    }

    /**
     * @return $this
     * @throws Zend_Http_Client_Exception
     */
    public function execute(): self
    {
        if (!$this->config->isCronEnabled()) {
            return $this;
        }

        $weatherDataFromAPI = $this->openWeatherMap->getWeather();

        $weather = $this->weatherRepositoryFactory->create();
        $weather->setData($this->helper->prepareData($weatherDataFromAPI));
        $this->weatherRepositoryInterface->save($weather);

        return $this;
    }
}
