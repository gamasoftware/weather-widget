<?php
/**
 * @category   GamaSoftware
 * @package    GamaSoftware_Weather
 * @subpackage Api
 * @author     Grzegorz Rzeźnikiewicz <grzegorz.rzeznikiewicz@gmail.com>
 * @copyright  Copyright (c) 2020 Gama Software
 * @since      1.0.0
 */

namespace GamaSoftware\Weather\Api\Data;

use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface WeatherRepositoryInterface
 *
 * @package GamaSoftware\Weather\Api\Data
 */
interface WeatherRepositoryInterface
{
    /**
     * @param WeatherInterface $weather
     *
     * @return void
     */
    public function save(WeatherInterface $weather): void;

    /**
     * @param int $id
     *
     * @return WeatherInterface
     */
    public function getById($id): WeatherInterface;

    /**
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return SearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria): SearchResultsInterface;

    /**
     * @param WeatherInterface $weather
     *
     * @return void
     */
    public function delete(WeatherInterface $weather): void;

    /**
     * @param int $id
     *
     * @return void
     */
    public function deleteById($id): void;

    /**
     * @return WeatherInterface
     */
    public function getLast(): WeatherInterface;
}
