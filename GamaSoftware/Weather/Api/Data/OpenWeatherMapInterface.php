<?php
/**
 * @category   GamaSoftware
 * @package    GamaSoftware_Weather
 * @subpackage Api
 * @author     Grzegorz Rzeźnikiewicz <grzegorz.rzeznikiewicz@gmail.com>
 * @copyright  Copyright (c) 2020 Gama Software
 * @since      1.0.0
 */

namespace GamaSoftware\Weather\Api\Data;

use Zend_Http_Response;

/**
 * Interface OpenWeatherMapInterface
 *
 * @package GamaSoftware\Weather\Api\Data
 */
interface OpenWeatherMapInterface
{
    public const API_ENDPOINT    = 'https://api.openweathermap.org/data/';
    public const API_VERSION     = '2.5/';
    public const API_GET_WEATHER = 'weather';

    // API PARAMS
    public const API_PARAM_APP_ID_NAME = 'appid';
    public const API_PARAM_LANG_NAME   = 'lang';
    public const API_PARAM_UNITS_NAME  = 'units';
    public const API_PARAM_CITY_NAME   = 'q';

    //API default values:
    public const API_PARAM_LANG  = 'pl';
    public const API_PARAM_UNITS = 'metric';

    /**
     * @param string $url
     * @param string $method
     * @param array  $params
     * @param null   $header
     *
     * @return Zend_Http_Response
     */
    public function call(string $url, string $method, array $params = [], $header = null): Zend_Http_Response;
}
