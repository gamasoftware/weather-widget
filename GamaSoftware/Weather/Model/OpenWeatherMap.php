<?php declare(strict_types = 1);
/**
 * @category   GamaSoftware
 * @package    GamaSoftware_Weather
 * @subpackage Model
 * @author     Grzegorz Rzeźnikiewicz <grzegorz.rzeznikiewicz@gmail.com>
 * @copyright  Copyright (c) 2020 Gama Software
 * @since      1.0.0
 */

namespace GamaSoftware\Weather\Model;

use GamaSoftware\Weather\Api\Data\OpenWeatherMapInterface;
use GamaSoftware\Weather\Helper\Config;
use Magento\Framework\HTTP\ZendClient;
use Magento\Framework\HTTP\ZendClientFactory;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\Webapi\Rest\Request;
use Zend_Http_Client;
use Zend_Http_Client_Exception;
use Zend_Http_Response;

/**
 * Class OpenWeatherMap
 *
 * @package GamaSoftware\Weather\Model
 */
class OpenWeatherMap implements OpenWeatherMapInterface
{
    /**
     * @var ZendClient
     */
    protected $client;

    /**
     * @var Config
     */
    protected $config;

    /**
     * @var Json
     */
    protected $serializer;

    /**
     * OpenWeatherMap constructor.
     *
     * @param ZendClientFactory $httpClient
     * @param Config            $config
     * @param Json              $serializer
     */
    public function __construct(ZendClientFactory $httpClient, Config $config, Json $serializer)
    {
        $this->client     = $httpClient->create();
        $this->config     = $config;
        $this->serializer = $serializer;
    }

    /**
     * @param array $params
     *
     * @return mixed
     * @throws Zend_Http_Client_Exception
     */
    public function getWeather(array $params = [])
    {
        $url     = OpenWeatherMapInterface::API_ENDPOINT . OpenWeatherMapInterface::API_VERSION . OpenWeatherMapInterface::API_GET_WEATHER;
        $params  = $this->getMissingParams($params);
        $weather = $this->call($url, Zend_Http_Client::GET, $params);

        return $this->serializer->unserialize($weather->getBody());
    }

    /**
     * @param array $params
     *
     * @return array
     */
    protected function getMissingParams(array $params): array
    {
        if (!array_key_exists(OpenWeatherMapInterface::API_PARAM_APP_ID_NAME, $params)) {
            $params[OpenWeatherMapInterface::API_PARAM_APP_ID_NAME] = $this->config->getApiKey();
        }

        if (!array_key_exists(OpenWeatherMapInterface::API_PARAM_LANG_NAME, $params)) {
            $params[OpenWeatherMapInterface::API_PARAM_LANG_NAME] = OpenWeatherMapInterface::API_PARAM_LANG;
        }

        if (!array_key_exists(OpenWeatherMapInterface::API_PARAM_UNITS_NAME, $params)) {
            $params[OpenWeatherMapInterface::API_PARAM_UNITS_NAME] = OpenWeatherMapInterface::API_PARAM_UNITS;
        }

        if (!array_key_exists(OpenWeatherMapInterface::API_PARAM_CITY_NAME, $params)) {
            $params[OpenWeatherMapInterface::API_PARAM_CITY_NAME] = $this->config->getCity() . ',' . $this->config->getCountry();
        }

        return $params;
    }

    /**
     * @param string $url
     * @param string $method
     * @param array  $params
     * @param null   $header
     *
     * @return Zend_Http_Response
     * @throws Zend_Http_Client_Exception
     */
    public function call(string $url, string $method, array $params = [], $header = null): Zend_Http_Response
    {
        if ($method === Request::HTTP_METHOD_GET) {
            $this->client->setParameterGet($params);
        } elseif ($method === Request::HTTP_METHOD_POST) {
            $this->client->setParameterPost($params);
        }

        $this->client->setUri($url)
                     ->setMethod($method)
                     ->setHeaders([
                                      'Content-Type: application/x-www-form-urlencoded',
                                      'Accept: application/json',
                                      'Key: ' . $header,
                                  ]);

        return $this->client->request();
    }
}
