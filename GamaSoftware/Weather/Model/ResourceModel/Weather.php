<?php declare(strict_types = 1);
/**
 * @category   GamaSoftware
 * @package    GamaSoftware_Weather
 * @subpackage Model
 * @author     Grzegorz Rzeźnikiewicz <grzegorz.rzeznikiewicz@gmail.com>
 * @copyright  Copyright (c) 2020 Gama Software
 * @since      1.0.0
 */

namespace GamaSoftware\Weather\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class Weather
 *
 * @package GamaSoftware\Weather\Model\ResourceModel
 */
class Weather extends AbstractDb
{
    /**
     * @return void
     */
    protected function _construct(): void
    {
        $this->_init('gama_software_weather', 'entity_id');
    }
}
