<?php declare(strict_types = 1);
/**
 * @category   GamaSoftware
 * @package    GamaSoftware_Weather
 * @subpackage Model
 * @author     Grzegorz Rzeźnikiewicz <grzegorz.rzeznikiewicz@gmail.com>
 * @copyright  Copyright (c) 2020 Gama Software
 * @since      1.0.0
 */

namespace GamaSoftware\Weather\Model;

use Exception;
use GamaSoftware\Weather\Api\Data\WeatherInterface;
use GamaSoftware\Weather\Api\Data\WeatherRepositoryInterface;
use GamaSoftware\Weather\Model\ResourceModel\Weather as WeatherResource;
use GamaSoftware\Weather\Model\ResourceModel\Weather\CollectionFactory;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Api\SearchResultsInterfaceFactory;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class WeatherRepository
 *
 * @package GamaSoftware\Weather\Model
 */
class WeatherRepository implements WeatherRepositoryInterface
{

    /**
     * @var WeatherResource
     */
    protected $weatherResource;

    /**
     * @var WeatherFactory
     */
    protected $weatherFactory;

    /**
     * @var CollectionFactory
     */
    protected $weatherCollectionFactory;

    /**
     * @var SearchResultsInterfaceFactory
     */
    protected $searchResultFactory;

    /**
     * @param WeatherResource               $weatherResource
     * @param WeatherFactory                $weatherFactory
     * @param CollectionFactory             $weatherCollectionFactory
     * @param SearchResultsInterfaceFactory $searchResultFactory
     */
    public function __construct(
        WeatherResource $weatherResource,
        WeatherFactory $weatherFactory,
        CollectionFactory $weatherCollectionFactory,
        SearchResultsInterfaceFactory $searchResultFactory
    ) {
        $this->weatherResource          = $weatherResource;
        $this->weatherFactory           = $weatherFactory;
        $this->weatherCollectionFactory = $weatherCollectionFactory;
        $this->searchResultFactory      = $searchResultFactory;
    }

    /**
     * @param WeatherInterface $weather
     *
     * @return void
     * @throws Exception
     * @throws AlreadyExistsException
     */
    public function save(WeatherInterface $weather): void
    {
        /** @var Weather $weatherModel */
        $weatherModel = $this->weatherFactory->create();
        if ($weather->getId()) {
            $this->weatherResource->load($weatherModel, $weather->getId());
        }
        $weatherModel->setData($weather->getData());
        $this->weatherResource->save($weatherModel);
    }

    /**
     * @param int $id
     *
     * @return WeatherInterface
     * @throws NoSuchEntityException
     */
    public function getById($id): WeatherInterface
    {
        /** @var Weather $weatherModel */
        $weatherModel = $this->weatherFactory->create();
        $this->weatherResource->load($weatherModel, $id);
        if (!$weatherModel->getId()) {
            throw new NoSuchEntityException();
        }
        return $weatherModel;
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return SearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria): SearchResultsInterface
    {
        /** @var WeatherResource\Collection $collection */
        $collection = $this->weatherCollectionFactory->create();
        //Helper methods for translating search criteria to collection filters etc.
        $this->addFiltersToCollection($searchCriteria, $collection);
        $this->addSortOrdersToCollection($searchCriteria, $collection);
        $this->addPagingToCollection($searchCriteria, $collection);

        /** @var SearchResultsInterface $searchResult */
        $searchResult = $this->searchResultFactory->create();

        return $this->buildSearchResult($searchCriteria, $searchResult, $collection);
    }

    /**
     * @param SearchCriteriaInterface    $searchCriteria
     * @param WeatherResource\Collection $collection
     */
    private function addFiltersToCollection(
        SearchCriteriaInterface $searchCriteria,
        WeatherResource\Collection $collection
    ): void {
        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            $fields = $conditions = [];
            foreach ($filterGroup->getFilters() as $filter) {
                $fields[]     = $filter->getField();
                $conditions[] = [$filter->getConditionType() => $filter->getValue()];
            }
            $collection->addFieldToFilter($fields, $conditions);
        }
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @param AbstractCollection      $collection
     */
    protected function addPagingToCollection(
        SearchCriteriaInterface $searchCriteria,
        AbstractCollection $collection
    ): void {
        $collection->setPageSize($searchCriteria->getPageSize());
        $collection->setCurPage($searchCriteria->getCurrentPage());
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @param SearchResultsInterface  $searchResults
     * @param AbstractCollection      $collection
     *
     * @return SearchResultsInterface
     */
    protected function buildSearchResult(
        SearchCriteriaInterface $searchCriteria,
        SearchResultsInterface $searchResults,
        AbstractCollection $collection
    ): SearchResultsInterface {
        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());

        return $searchResults;
    }

    /**
     * @param WeatherInterface $weather
     *
     * @return void
     * @throws Exception
     */
    public function delete(WeatherInterface $weather): void
    {
        $this->deleteById($weather->getId());
    }

    /**
     * @param int $id
     *
     * @return void
     * @throws Exception
     */
    public function deleteById($id): void
    {
        /** @var Weather $weatherModel */
        $weatherModel = $this->weatherFactory->create();

        $this->weatherResource->load($weatherModel, $id);
        $this->weatherResource->delete($weatherModel);
    }

    /**
     * @return WeatherInterface
     */
    public function getLast(): WeatherInterface
    {
        /** @var WeatherResource\Collection $collection */
        $collection = $this->weatherCollectionFactory->create();
        /**
         * @var $weather WeatherInterface
         */
        $weather = $collection->getLastItem();

        return $weather;
    }
}
