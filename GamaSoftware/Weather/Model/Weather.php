<?php declare(strict_types = 1);
/**
 * @category   GamaSoftware
 * @package    GamaSoftware_Weather
 * @subpackage Model
 * @author     Grzegorz Rzeźnikiewicz <grzegorz.rzeznikiewicz@gmail.com>
 * @copyright  Copyright (c) 2020 Gama Software
 * @since      1.0.0
 */

namespace GamaSoftware\Weather\Model;

use GamaSoftware\Weather\Api\Data\WeatherInterface;
use GamaSoftware\Weather\Model\ResourceModel;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;

/**
 * Class Weather
 *
 * @package GamaSoftware\Weather\Model
 */
class Weather extends AbstractModel implements IdentityInterface, WeatherInterface
{
    public const CACHE_TAG = 'gama_software_weather';

    /**
     * @var WeatherFactory
     */
    protected $weatherDataFactory;

    /**
     * @param WeatherFactory        $weatherDataFactory
     * @param Context               $context
     * @param Registry              $registry
     * @param AbstractResource|null $resource
     * @param AbstractDb|null       $resourceCollection
     * @param array                 $data
     */
    public function __construct(
        WeatherFactory $weatherDataFactory,
        Context $context,
        Registry $registry,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);

        $this->weatherDataFactory = $weatherDataFactory;
    }

    /**
     * @return void
     */
    public function _construct()
    {
        $this->_init(ResourceModel\Weather::class);
    }

    /**
     * @inheritDoc
     */
    public function getIdentities()
    {
        return [static::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * @inheritDoc
     */
    public function getDate(): string
    {
        return (string)$this->getData(static::DATE);
    }

    /**
     * @inheritDoc
     */
    public function setDate(string $date): void
    {
        $this->setData(static::DATE, $date);
    }

    /**
     * @inheritDoc
     */
    public function getLocalization(): ?string
    {
        return (string)$this->getData(static::LOCALIZATION);
    }

    /**
     * @inheritDoc
     */
    public function setLocalization(string $localization): void
    {
        $this->setData(static::LOCALIZATION, $localization);
    }

    /**
     * @inheritDoc
     */
    public function getCity(): ?string
    {
        return (string)$this->getData(static::CITY);
    }

    /**
     * @inheritDoc
     */
    public function setCity(string $city): void
    {
        $this->setData(static::CITY, $city);
    }

    /**
     * @inheritDoc
     */
    public function getTemp(): ?float
    {
        return $this->getData(static::TEMP);
    }

    /**
     * @inheritDoc
     */
    public function setTemp($temp): void
    {
        $this->setData(static::TEMP, $temp);
    }

    /**
     * @inheritDoc
     */
    public function getTempMin(): ?float
    {
        return $this->getData(static::TEMP_MIN);
    }

    /**
     * @inheritDoc
     */
    public function setTempMin($temp_min): void
    {
        $this->setData(static::TEMP_MIN, $temp_min);
    }

    /**
     * @inheritDoc
     */
    public function getTempMax(): ?float
    {
        return $this->getData(static::TEMP_MAX);
    }

    /**
     * @inheritDoc
     */
    public function setTempMax($temp_max): void
    {
        $this->setData(static::TEMP_MAX, $temp_max);
    }

    /**
     * @return int
     */
    public function getPressure(): ?int
    {
        return $this->getData(static::PRESSURE);
    }

    /**
     * @inheritDoc
     */
    public function setPressure($pressure): void
    {
        $this->setData(static::PRESSURE, $pressure);
    }

    /**
     * @inheritDoc
     */
    public function getHumidity(): ?int
    {
        return $this->getData(static::HUMIDITY);
    }

    /**
     * @inheritDoc
     */
    public function setHumidity($humidity): void
    {
        $this->setData(static::HUMIDITY, $humidity);
    }

    /**
     * @inheritDoc
     */
    public function getSunrise(): ?string
    {
        return (string)$this->getData(static::SUNRISE);
    }

    /**
     * @inheritDoc
     */
    public function setSunrise($sunrise): void
    {
        $this->setData(static::SUNRISE, $sunrise);
    }

    /**
     * @inheritDoc
     */
    public function getSunset(): ?string
    {
        return (string)$this->getData(static::SUNSET);
    }

    /**
     * @inheritDoc
     */
    public function setSunset($sunset): void
    {
        $this->setData(static::SUNSET, $sunset);
    }

    /**
     * @inheritDoc
     */
    public function getClouds(): ?int
    {
        return $this->getData(static::CLOUDS);
    }

    /**
     * @inheritDoc
     */
    public function setClouds($clouds): void
    {
        $this->setData(static::CLOUDS, $clouds);
    }

    /**
     * @inheritDoc
     */
    public function getVisibility(): ?int
    {
        return $this->getData(static::VISIBILITY);
    }

    /**
     * @inheritDoc
     */
    public function setVisibility($visibility): void
    {
        $this->setData(static::VISIBILITY, $visibility);
    }

    /**
     * @inheritDoc
     */
    public function getWindSpeed(): ?float
    {
        return $this->getData(static::WIND_SPEED);
    }

    /**
     * @inheritDoc
     */
    public function setWindSpeed($wind_speed): void
    {
        $this->setData(static::WIND_SPEED, $wind_speed);
    }

    /**
     * @inheritDoc
     */
    public function getWindGust(): ?float
    {
        return $this->getData(static::WIND_GUST);
    }

    /**
     * @inheritDoc
     */
    public function setWindGust($wind_gust): void
    {
        $this->setData(static::WIND_GUST, $wind_gust);
    }

    /**
     * @inheritDoc
     */
    public function getWindDeg(): ?int
    {
        return $this->getData(static::WIND_DEG);
    }

    /**
     * @inheritDoc
     */
    public function setWindDeg($wind_deg): void
    {
        $this->setData(static::WIND_DEG, $wind_deg);
    }

    /**
     * @inheritDoc
     */
    public function getDescription(): ?string
    {
        return $this->getData(static::DESCRIPTION);
    }

    /**
     * @inheritDoc
     */
    public function setDescription(string $description): void
    {
        $this->setData(static::DESCRIPTION, $description);
    }

    /**
     * @inheritDoc
     */
    public function getMain(): ?string
    {
        return $this->getData(static::MAIN);
    }

    /**
     * @inheritDoc
     */
    public function setMain(string $main): void
    {
        $this->setData(static::MAIN, $main);
    }

    /**
     * @inheritDoc
     */
    public function getIcon(): ?string
    {
        return $this->getData(static::ICON);
    }

    /**
     * @inheritDoc
     */
    public function setIcon(string $icon): void
    {
        $this->setData(static::ICON, $icon);
    }
}
